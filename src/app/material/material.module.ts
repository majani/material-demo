import { NgModule } from '@angular/core';
import { MatButtonModule, MatButtonToggleModule, MatSliderModule, MatCardModule, MatTableModule } from '@angular/material';

const MaterialComponents = [
  MatButtonModule,
  MatButtonToggleModule,
  MatSliderModule,
  MatCardModule,
  MatTableModule
]



@NgModule({
  imports: [
    MaterialComponents
  ],

  exports: [
    MaterialComponents
  ]
})
export class MaterialModule { }
